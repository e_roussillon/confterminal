#!/bin/bash
if [ "$#" -eq  "0" ]
   then
     ssh -i $HOME/Documents/Elixir/tests/panda_phoenix_configs/dotpandaKey.pem dotpanda@ec2-34-224-71-170.compute-1.amazonaws.com
 else
     ssh -i $HOME/Documents/Elixir/tests/panda_phoenix_configs/dotpandaKey.pem $1@ec2-34-224-71-170.compute-1.amazonaws.com
 fi
